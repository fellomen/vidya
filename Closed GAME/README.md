# Closed GAME
## Visual guides for [Closed GAME](https://jastusa.com/games/jast032/closed-game) (Jast USA version).
Based on some japanese visual guide, uses the correctly translated choices from the Jast USA version.
Also includes an extended version of the guide, showing you which scene (numbered by their appearance in `Extras > Scene Replay`).
Available in light and dark "themes".

## Save file
The save files can be found in `C:\Program Files\Closed Game\savedata\`.
The "system" file which stores extra unlocks (scenes, images, jukebox, etc.) is `majiro_system.mss`. The file in this directory is a 100% save.

## Linux/Wine
To get it running with Wine, I had to delete/rename the OP movie file (`C:\Program Files\Closed Game\Closed GAME_OP.wmv`). Not sure if there's another fix to this, but this was simple enough for me.