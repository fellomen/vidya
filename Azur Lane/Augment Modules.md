# Augment Modules

### Other sources/guides
- Anon's Comprehensive Augment Guide: https://pastebin.com/UBpjkhFg
- Datamine Spreadsheet: https://docs.google.com/spreadsheets/d/e/2PACX-1vThXBUZOCaOma3_Fy94S6p_axdvLx3SiyWrndljYAj3ZYYFBmFJlJSPzk0_sGSMEv4g8Sxea3B1h2RT/pubhtml
- EN Wiki: https://azurlane.koumakan.jp/wiki/User:Samheart564/Augment_Module_Guide

## README

- You can craft/view augment modules from: `Depot > Craft/Gear > Special (on the left bar)` or by selecting any MLB ship and adding augment gear, which also only displays the items that she can equip.
- Items are dropped from:
	- weekly raid (twice): augments, module cores, augment module stones, and augment module conversion stones
	- Cruise Missions: augment module stones (T2 in free and T3 in paid), augment module cores (T2 in free and paid)
	- Arbiter fight: augment module cores
	- map drops: augment module stones
- You can "uncap" a rare augment to an elite augment by upgrading it to +10 and then paying an additional 2 `Augment Module Cores` + 1000 coins. This makes the cost of uncapping a rare augment the same as building an elite augment from scratch. It's cheaper to uncap a rare obtained from the weekly raid than crafting an elite though.
- `Module Effects` are only unlocked for elite augments at +10, rare augments do not unlock their `Module Effect` at all.
- For SR augments the `Module Effect` is unlocked instantly after bulding, but the `Skill Upgrade` only unlocks at +10.
- Augments come with a fixed stat boost and random stat boost. The random stat boost can be upgraded via the `Convert` button on the item and consumes 1 `Augment Module Conversion Stone` (T1 for rare and elite augments, T2 for SR augments). Each conversion increases the random stat by at least 1.

## What do I put on my ships? tl;dr
Courtesy of anon.
- For ships that have unique augments use those for their skill upgrades.
- *BB/BC/BBV*:
  - Bowgun vs heavy (effect bonus, +5% DMG) and medium (for the FP)
  - Officer Sword vs light (bowgun nerfs damage vs light, -5% DMG) (Hit/EVA is less useful but still a nice bonus)
- *CV/CVL*: Scepter on CV that launches fastest, Bows on the other 2 CVs
- *CL*: Crossbow for FP boost, Sword for defensive utility/screenclear
- *DD*: Dual swords for gun-focused DDs, Hammer for torpedo-focused DDs
- *CB/CA*: Greatsword, FP/TRP is the superior choice + screenclear
- *SS/SSV*: Dagger, always for the TRP Crit Rate
- *BM* and *AE*: Lance, only augment for BM and AE.
- *AR*: Crossbow, only augment for AR.

## Crafting and upgrade costs

| Rarity | crafting                          | +0 → +10             | Total                            |
| :-:    | :-:                               | :-:                  | :-:                              |
| SR     | 3000 XP + 10000 coins + 15 cores  | 3120 XP + 9000 coins | 6120 XP + 19000 coins + 15 cores |
| E      | 2000 XP +  2800 coins +  4 cores  |  780 XP + 2200 coins | 2780 XP +  5000 coins +  4 cores |
| R      |  250 XP +   700 coins +  2 cores  |  750 XP + 1100 coins | 1000 XP +  1800 coins +  2 cores |

## XP
| Item name                | Rarity | XP   |
| :-:                      | :-:    | :-:  |
| Augment Module Stone T3  | SR     | 1000 |
| Augment Module Stone T2  | E      |  100 |
| Augment Module Stone T1  | R      |   10 | 
| any SR augment module*   | E      | 1800 |
| any elite augment module | E      |  600 |
| any rare augment module  | R      |  150 |

\* requires you to input the full name (case-sensitive) of the module before it can be disassembled.

## Area slash vs Spread slash
Courtesy of anon.
![area slash vs spread slash visual](https://files.catbox.moe/booefj.png)

## Stacking multiples of the same slash type
Stacking multiple slash attacks of the same type (ie. multiple `Slash Attack - Spread`) in one fleet upgrades the attack.

### Spread
| Lv. | Effect bonus (cumulative)             |
| :-: | :--                                   |
| 1   | -                                     |
| 2   | Fires two projectiles                 |
| 3   | Second projectil inflicts Armor Break |

#### Area
| Lv. | Effect bonus (cumulative)             |
| :-: | :--                                   |
| 1   | -                                     |
| 2   | Increased AoE radius                  |
| 3   | Inflicts 20% slow for 5 seconds       |

Comparission between Area Lv. 1 and Area Lv. 2 AoE radius (courtesy of anon):
![1 sword vs 2 swords visual](https://files.catbox.moe/c68juv.png)

## Full equipment list

### Uniques (SR)
| Ship        | Icon                                                | Item                             | Stat boost 1 | 2          | Module Effect               | Skill Upgrade            |
| :-:         | :-:                                                 | :-:                              | :-:          | :-:        | :-:                         | :-:                      |
| I-58        | ![I-58](https://files.catbox.moe/gv680l.png)        | Hydrophone Calibration Device    | TRP 22 + 10  | RLD 7 + 5  | Improved Air Tanks          | Departing Strike+        |
| Arizona     | ![Arizona](https://files.catbox.moe/s3o3wn.png)     | No More Tears                    | FP 25 + 10   | RLD 10 + 5 | Fast Reloading              | Eagle's Tears+           |
| Prinz Eugen | ![Prinz Eugen](https://files.catbox.moe/dhu55g.png) | Lucky Ship's "Punishers"         | EVA 20 + 10  | FP 20 + 5  | Anti-Armor Rounds           | Unbreakable Shield+      |
| Belfast     | ![Belfast](https://files.catbox.moe/9wb2w7.png)     | Afternoon of Surging Elegance    | FP 22 + 10   | RLD 15 + 5 | Slash Attack - Area         | Burn Order+              |
| Laffey      | ![Laffey](https://files.catbox.moe/gd3n02.png)      | Huggy Pillow of Bravery          | FP 22 + 10   | HIT 13 + 5 | Slash Attack - Spread       | Wargod of Solomon+       |
| Jean Bart   | ![Jean Bart](https://files.catbox.moe/iyajml.png)   | Templar Knight's Battle Standard | FP 25 + 10   | HIT 6 +5   | Counterattack - Firepower   | Final Shot+              |
| Glorious    | ![Glorious](https://files.catbox.moe/cz0al6.png)    | Punctual Glory                   | AVI 25 + 10  | AA 17 + 5  | Emergency Sortie - Aircraft | Fleet Carrier - Glorious |

### BB/BC/BBV
| Icon                                                    | Item            | Stat boost 1 | 2          | Module Effect                 |
| :-:                                                     | :-:             | :-:          | :-:        | :-:                           |
| ![Bowgun](https://files.catbox.moe/ni3n0o.png)          | Bowgun          | FP 20 + 5    | AA 17 + 3  | Ammo Calibration Pentration   |
| ![Officer's Sword](https://files.catbox.moe/rn5aqn.png) | Officer's Sword | Hit 15 + 5   | EVA 17 + 3 | First Strike - Firepower      |

### BM
| Icon                                                    | Item            | Stat boost 1 | 2          | Module Effect                 |
| :-:                                                     | :-:             | :-:          | :-:        | :-:                           |
| ![Lance](https://files.catbox.moe/zab2ky.png)           | Lance           | Hit 19 + 5   | AA 18 + 3  | Counterattack - Firepower     |

### CV/CVL
| Icon                                                    | Item            | Stat boost 1 | 2          | Module Effect                 |
| :-:                                                     | :-:             | :-:          | :-:        | :-:                           |
| ![Scepter](https://files.catbox.moe/b5zkn7.png)         | Scepter         | AVI 25 + 5   | EVA 17 + 3 | First Strike - Aviation       |
| ![Hunting Bow](https://files.catbox.moe/6s5dfa.png)     | Hunting Bow     | AVI 25 + 5   | AA 17 + 3  | Air Superiority Tactics       |

### CL
| Icon                                                    | Item            | Stat boost 1 | 2          | Module Effect                 |
| :-:                                                     | :-:             | :-:          | :-:        | :-:                           |
| ![Sword](https://files.catbox.moe/r6ctrr.png)           | Sword           | Hit 15 + 5   | RLD 21 + 3 | Slash Attack - Spread         |
| ![Crossbow](https://files.catbox.moe/80wwm3.png)        | Crossbow        | FP 15 + 5    | AA 21 + 3  | Damage Reduction - Aircraft   |

### CB/CA
| Icon                                                    | Item            | Stat boost 1 | 2          | Module Effect                 |
| :-:                                                     | :-:             | :-:          | :-:        | :-:                           |
| ![Greatsword](https://files.catbox.moe/tq70xq.png)      | Greatsword      | FP 20 + 5    | TRP 15 + 5 | Slash Attack - Area           |
| ![Lance](https://files.catbox.moe/zab2ky.png)           | Lance           | Hit 19 + 5   | AA 18 + 3  | Counterattack - Firepower     |

### DD
| Icon                                                    | Item            | Stat boost 1 | 2          | Module Effect                 |
| :-:                                                     | :-:             | :-:          | :-:        | :-:                           |
| ![Dual Swords](https://files.catbox.moe/0xthgp.png)     | Dual Swords     | FP 20 + 5    | EVA 12 + 3 | Slash Attack - Area           |
| ![Hammer](https://files.catbox.moe/r44kpn.png)          | Hammer          | TRP 30 + 5   | Hit 7 + 3  | Slash Attack - Spread         | 

### SS/SSV
| Icon                                                    | Item            | Stat boost 1 | 2          | Module Effect                 |
| :-:                                                     | :-:             | :-:          | :-:        | :-:                           |
| ![Dagger](https://files.catbox.moe/x1v3s1.png)          | Dagger          | Hit 10 + 5   | EVA 15 + 3 | Critical Targeting - Torpedos |
| ![Kunai](https://files.catbox.moe/tcqzux.png)           | Kunai           | TRP 35 + 5   | Hit 5 + 3  | Crippling Blow - Torpedoes    | 

### AR
| Icon                                                    | Item            | Stat boost 1 | 2          | Module Effect                 |
| :-:                                                     | :-:             | :-:          | :-:        | :-:                           |
| ![Crossbow](https://files.catbox.moe/80wwm3.png)        | Crossbow        | FP 15 + 5    | AA 21 + 3  | Damage Reduction - Aircraft   |

### AE
| Icon                                                    | Item            | Stat boost 1 | 2          | Module Effect                 |
| :-:                                                     | :-:             | :-:          | :-:        | :-:                           |
| ![Lance](https://files.catbox.moe/zab2ky.png)           | Lance           | Hit 19 + 5   | AA 18 + 3  | Counterattack - Firepower     |
