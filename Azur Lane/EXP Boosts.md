# EXP-Boosting Ships

|   |   | 
|:-:|:--|
| ![Eagle Union][Eagle Union] ![CVL][CVL] **[Langley][Wiki Langley]** | Boosts:  ![CVL][CVL] ![CV][CV] |
| ![Langley][Chibi Langley] | **First-Generation Carrier** increases EXP gained by all CVs and CVLs by (up to) 15%.<br />Works well for the following PR ships:<br />[PR2: Georgia][Wiki Georgia]<br />[PR3: Champagne][Wiki Champagne]<br />[PR5: Chkalov (Phase 2)][Wiki Chkalov]<br />[PR6: Kearsage][Wiki Kearsage] | 
| ![Sakura Empire][Sakura Empire] ![CVL][CVL] **[Houshou][Wiki Houshou]** | Boosts:  ![CVL][CVL] ![CV][CV] |
| ![Houshou][Chibi Houshou] | **First-Generation Carrier** increases EXP gained by all CVs and CVLs by (up to) 15%.<br />Works well for the following PR ships:<br />[PR1: Izumo][Wiki Izumo]<br />[PR2: Azuma (Phase 1)][Wiki Azuma]<br />[PR4: Hakuryuu][Wiki Hakuryuu] |
| ![Sakura Empire][Sakura Empire] ![CL][CL] **[Yuubari][Wiki Yuubari]** | Boosts: ![CL][CL] ![CA][CA] ![CB][CB]  |
| ![Yuubari][Chibi Yuubari] | **Prototype Cruiser** increases EXP gained by all Cruisers by (up to) 15%.<br />Works well for the following PR ships:<br />[PR1: Ibuki][Wiki Ibuki]<br />[PR2: Kitakaze][Wiki Kitakaze]<br />[PR2: Azuma (Phase 2)][Wiki Azuma]<br />[PR6: Shimanto][Wiki Shimanto] |
| ![Royal Navy][Royal Navy] ![DD][DD] **[Amazon][Wiki Amazon]** | Boosts: ![DD][DD] |
| ![Amazon][Chibi Amazon] | **Prototype Destroyer** increases EXP gained by all Destroyers by (up to) 18%.<br />Works well for the following PR ships:<br />[PR1: Neptune][Wiki Neptune]<br />[PR3: Cheshire][Wiki Cheshire]<br />[PR3: Drake][Wiki Drake]<br />[PR5: Plymouth][Wiki Plymouth] |
| ![Iron Blood][Iron Blood] ![CL][CL] **[Nürnberg][Wiki Nürnberg]** | Boosts: ![SS][SS] ![SSV][SSV]
| ![Nürnberg][Chibi Nürnberg] | **Follow My Lead... Please!** increases EXP gained by all SS and SSVs by (up to) 10%. |


**🛈 All of the skills only buff EXP of ships in the same fleet.**

**🛈 The buffs of *Langley* and *Houshou* can be stacked, if you want to powerlevel one other carrier.**

**🛈 If your ships are in a ![good][good mood] mood (morale >= 120), they earn +20% EXP.**
> Make sure that the ships you are actively using for EXP farming are in the dorm (ideally 2nd floor).

**🛈 Watch out for (preparation phase) events which boost specific ships EXP.**
> Usually +20% EXP, for example: [Call to Arms: Royal Navy](https://azurlane.koumakan.jp/wiki/Call_to_Arms:_Royal_Navy)


[CA]: https://files.catbox.moe/uwodpu.png
[CB]: https://files.catbox.moe/4j5uol.png
[CL]: https://files.catbox.moe/r2gyrp.png
[CV]: https://files.catbox.moe/myhpku.png
[CVL]: https://files.catbox.moe/5i7jss.png
[DD]: https://files.catbox.moe/9sy98w.png
[SS]: https://files.catbox.moe/vpz9z3.png
[SSV]: https://files.catbox.moe/iyfgmg.png

[Chibi Amazon]: https://files.catbox.moe/1bb8cq.png
[Chibi Houshou]: https://files.catbox.moe/9hkc4e.png
[Chibi Langley]: https://files.catbox.moe/7lllrk.png
[Chibi Nürnberg]: https://files.catbox.moe/2xsck5.png
[Chibi Yuubari]: https://files.catbox.moe/yjsufc.png

[Eagle Union]: https://files.catbox.moe/q8gxn0.png
[Iron Blood]: https://files.catbox.moe/czaiad.png
[Royal Navy]: https://files.catbox.moe/31ybrm.png
[Sakura Empire]: https://files.catbox.moe/bz82fp.png

[Wiki Amazon]: https://azurlane.koumakan.jp/wiki/Amazon
[Wiki Azuma]: https://azurlane.koumakan.jp/wiki/Azuma
[Wiki Champagne]: (https://azurlane.koumakan.jp/wiki/Champagne
[Wiki Cheshire]: https://azurlane.koumakan.jp/wiki/Cheshire
[Wiki Drake]: https://azurlane.koumakan.jp/wiki/Drake
[Wiki Georgia]: https://azurlane.koumakan.jp/wiki/Georgia
[Wiki Hakuryuu]: https://azurlane.koumakan.jp/wiki/Hakuryuu
[Wiki Houshou]: https://azurlane.koumakan.jp/wiki/Houshou
[Wiki Ibuki]: https://azurlane.koumakan.jp/wiki/Ibuki
[Wiki Izumo]: https://azurlane.koumakan.jp/wiki/Izumo
[Wiki Kitakaze]: https://azurlane.koumakan.jp/wiki/Kitakaze
[Wiki Langley]: https://azurlane.koumakan.jp/wiki/Langley
[Wiki Neptune]: https://azurlane.koumakan.jp/wiki/Neptune
[Wiki Nürnberg]: https://azurlane.koumakan.jp/wiki/N%C3%BCrnberg
[Wiki Yuubari]: https://azurlane.koumakan.jp/wiki/Yuubari
[Wiki Plymouth]: https://azurlane.koumakan.jp/wiki/Plymouth
[Wiki Chkalov]: https://azurlane.koumakan.jp/wiki/Chkalov
[Wiki Kearsage]: https://azurlane.koumakan.jp/wiki/Kearsarge
[Wiki Shimanto]: https://azurlane.koumakan.jp/wiki/Shimanto

[good mood]: https://files.catbox.moe/mrru53.png
