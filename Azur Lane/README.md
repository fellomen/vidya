# Azur Lane

## EXP overflow lookup table

| Lv. range | Normal    | Ultra Rare |
| :-------: | --------: | ---------: |
| 100 > 120 | 2,105,000 |  2,538,000 |
| 105 > 120 | 1,825,000 |  2,202,000 |
| 110 > 120 | 1,440,000 |  1,740,000 |
| 115 > 120 |   865,000 |  1,050,000 |
| 120 > 125 | 1,310,000 |  1,572,000 |

## Trackers/Checklists

### Retrofit/Blueprints
Interactive checklist for tracking blueprints for retrofits:
* OpenDocument Spreadsheet file: [Retrofit Blueprint Tracker.fods](Retrofit Blueprint Tracker.fods)
* or as [Google Spreadsheets](https://docs.google.com/spreadsheets/d/1iDZgFXC7l4_nxzwF1d0mRBFAVJdS00iU5kpGcZ4QWIM/).

### META Ships
* OpenDocument Spreadsheet file: [META Tracker.fods](META Tracker.fods)
* or as [Google Spreadsheets](https://docs.google.com/spreadsheets/d/1Ddlviijx2a1Cn0aFegh_Lr6pWk9ZqixQNxg5SEMH1us).

### Augment Modules.md
Just a small write up on augment modules. Can also be viewed [here](https://rentry.org/td943).

### EXP Boosts.md & EXP Boosts (rentry).md
* Small write-up on ships with EXP boosting skills, as well as some other tips for EXP boosts.
* The `EXP Boosts.md` version is adjusted to work with gitlab flavored markdown, allthough Gitlab refuses to load images from catbox, but it should be more portable in general.
* The `EXP Boosts (rentry).md` version is built to be viewd on rentry ([link](https://rentry.org/3vf6b)). Also abuses centering markdown(`->` and `<-`) to force new lines in a table cell, because rentry doesn't render html tags in markdown.


## Skill books

| Book | EXP (w/o Bonus) | Duration | XP/hour (w/o Bonus) | Amount & Time to Lv. 10 |
| :-:  | --:             | --:      | --:                 | --:                     |
| T1   |  150  (100)     |  2:00    |  75  (50)           | 124 ~ 248h              |
| T2   |  450  (300)     |  4:00    | 113  (75)           |  42 ~ 168h              |
| T3   | 1200  (800)     |  8:00    | 150 (100)           |  16 ~ 128h              |
| T4   | 3500 (1500)     | 12:00    | 292 (125)           |   6 ~  72h              |


* EXP required from 1 → 10: 18500 EXP
* Assuming you can always queue up another book when the previous one finishes:
  * It's better to use an on-color T3 book than to use an off-color T4 book.
  * It's better to use an on-color T2 book than to use an off-color T3 book.
  * T1 on-color and T2 off-color yield the same xp/h.
  * It's always better to use an off-color T3 book than an on-color T1 book.
* If you can't queue up another book when the previous one finishes:
  * Calculating when an off-color book is better than an on-color book of a lower tier:
    * $`\frac{exp}{t} > xph \implies exp > xph \times t \implies \frac{exp}{xph} > t`$.
    * `exp ...` amount of EXP the off-color book gives
    * `t .....` time required for it to be worth it over the on-color book (in hours)
    * `xph ...` xp/h of the on-color book
  * It's better to use an off-color T3 book over an on-color T2 book, if you can't check the game within 7 hours 24 minutes.
     * $`\frac{exp}{xph} > t \implies \frac{800}{113} > t \implies  7.08 > t`$
  * It's better to use an off-color T2 book over an on-color T1 book, if you cant't check the game within 4h.
     * $`\frac{exp}{xph} > t \implies \frac{300}{75} > t \implies  4 > t`$


