#!/bin/bash
echo "Creating backup of Elden Ring save folder and graphics config file..."
backup_file="eldenring_backup_$(date +'%Y%m%d%H%M')"
dest="$PWD/$backup_file"
cd ~/.local/share/Steam/steamapps/compatdata/1245620/pfx/drive_c/users/steamuser/AppData/Roaming/EldenRing/
if zip -rq "$dest" . ; then
    echo "Created backup zip: $backup_file.zip"
else
    echo "Creating backup failed."
fi
cd - 2>&1 >/dev/null
