## Monster Hunter Rise



### [backup_mhr_save.sh](backup_mhr_save.sh)
Shell script to zip and back up MHR's save files. This includes all photo files from your ingame camera.

### [delete_mhr_shader_cache.sh](delete_mhr_shader_cache.sh)
In the base game the shader cache sometimes caused issues when playing online (disconnects when loading into quests).
To fix that this script deletes the shader cache file and forces the game to rebuild it on startup.
Doesn't seem to be required with Sunbreak anymore.


### Misc notes that don't deserve their own file

#### Dango Weakener
Increases the chance for the target monster to spawn with a smaller health pool.

For example in [MR 6★ A Single Beam of Moonlight](https://mhrise.kiranico.com/data/quests/1419360004) Gold Rathian can spawn with either 49950 HP, 51750 HP, 53550 HP, 55350 HP, or 57150 HP.

It only works when the host eats for it.

It does **not** work when the target monster(s) have a fixed HP pool, which is true for:
- Multi-monster hunts
- Arena quests
- Afflicted investigations (It **does work** on normal Anomaly quests)
- Quest for Gaismagorm, Narwa, Ibushi
- Village quests

You can check if it works on a specific quest on kiranico's page for the quest.
