#!/bin/bash
USER_ID="<YOUR STEAM USER ID HERE>"
echo "Creating backup of Monster Hunter Rise save folder..."
if zip -jrq "mhr_backup_$(date +'%Y%m%d%H%M')" ~/.local/share/Steam/userdata/$USER_ID/1446780/remote/win64_save/ ; then
    echo "done!"
else
    echo "failed."
fi
